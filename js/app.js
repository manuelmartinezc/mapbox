"use stirck"

mapboxgl.accessToken = "pk.eyJ1IjoibWFudWVsbWFydGluZXpjIiwiYSI6ImNrMGJrYXJxdjB2YzYzY2w2a2w1NTM1M2gifQ.1J1hmB6AL76LAxrTKTGWUw";

let map = new mapboxgl.Map({
    container:"mapa",
    style: 'mapbox://styles/mapbox/streets-v11',
    center:[-58.366345, -34.612928],
    zoom: 10

});

function isNullOrUndefined(val) {
    return (typeof val == 'undefined' || val == null);
}

function inputVal(elementId) {

    var elm= document.getElementById(elementId);
    if(isNullOrUndefined(elm)) {
        return "";
    }

    return elm.value;
}
 
function cargarDatos(event) {

    event.preventDefault();

    var datosForm ={
        nombre : inputVal("nombre"),
        direccion : inputVal("direccion"),
        telefono :  inputVal("telefono"),
        categoria : inputVal("categoria"),
        lat: inputVal("lat"),
        lng: inputVal("lng"),
        coordinates : [],
        MarkerText: function() {
            return `Descripcion: ${this.nombre} <br>  Direccion:  ${this.direccion} <br> Telefono:  ${this.telefono}<br> (X,Y):  ${this.coordinates} <br>Categoria:  ${this.categoria}`;
        }
    };

    console.log(datosForm);
    if(validar(datosForm) == false) {
        return;
    }

    datosForm.coordinates= [datosForm.lng, datosForm.lat];

    // Limpiar form
    document.getElementById("nombre").value ="";
    document.getElementById("direccion").value="";
    document.getElementById("telefono").value ="";
    document.getElementById("lng").value ="";
    document.getElementById("lat").value ="";

    
    geojson.datos.push(datosForm);
    marcador();
}


var geojson = {
    datos:[]
 
};




  function marcador(){

    
    geojson.datos.forEach(function(marker) {

            console.log(marker);
        var popup =  new mapboxgl.Popup({offset:25}).setHTML(marker.MarkerText());

        var el = document.createElement('div');
        el.className = 'marker';
        
        new mapboxgl.Marker(el)

          .setLngLat(marker.coordinates)
          .setPopup(popup)
          .addTo(map);
      });
    
  }

/*function validar(){
    var nombre
    var nombre = document.getElementById("nombre").value;
    var boton = document.getElementById("boton");
    var direccion =document.getElementById("direccion").value;
    var telefono =document.getElementById("telefono").value;
    var categoria =document.getElementById("categoria").value;
    var lng = parseFloat(document.getElementById("lng").value);
    var lat = parseFloat(document.getElementById('lat').value);
    if(nombre == "" ){
        alert("prueba");
        boton.disabled = true;
        return false;-

    }else{
        var nombre = document.getElementById("nombre").value; 
    }
    
}*/
 function validar(datosForm) {
     
    if(datosForm.nombre == "" || datosForm.direccion == "" || datosForm.telefono == "" || datosForm.categoria == "") {
        alert("Llenar todos los campos");
        return false;
    }

    if(!(datosForm.lng > -180 && datosForm.lng < 180)) {
        alert("Longitud no valida");
        return false;
    }

    if(!(datosForm.lat>-90 && datosForm.lat<90)){
        alert("Latitud no valida");
        return false;
    }
     
    return true;
 }

 



  





 